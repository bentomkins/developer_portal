import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'get-started',
    data: { label: 'Get Started' },
    loadChildren: () => import('./sections/get-started/get-started.module').then(module => module.GetStartedModule),
  },
  {
    path: 'cheat-sheet',
    data: { label: 'Cheat Sheet' },
    loadChildren: () => import('./sections/cheat-sheet/cheat-sheet.module').then(module => module.CheatSheetModule),
  },
  {
    path: 'syntax-highlight',
    data: { label: 'Syntax Highlight' },
    loadChildren: () => import('./sections/syntax-highlight/syntax-highlight.module').then(module => module.SyntaxHighlightModule),
  },
  {
    path: 'bindings',
    data: { label: 'Bindings' },
    loadChildren: () => import('./sections/bindings/bindings.module').then(module => module.BindingsModule),
  },
  {
    path: 'plugins',
    data: { label: 'Plugins' },
    loadChildren: () => import('./sections/plugins/plugins.module').then(module => module.PluginsModule),
  },
  {
    path: '**',
    redirectTo: 'get-started',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      anchorScrolling: 'enabled',
      scrollOffset: [0, 64],
      scrollPositionRestoration: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
