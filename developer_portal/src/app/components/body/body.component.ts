import { Component, HostListener, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ROUTE_ANIMATION } from '../../app.animation';
import { AnchorService } from '../../shared/anchor/anchor.service';

@Component({
  animations: [ROUTE_ANIMATION],
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss'],
})
export class BodyComponent implements OnInit {


  @HostListener('document:click', ['$event'])
  onDocumentClick(event: Event): void {
    this.anchorService.interceptClick(event);
  }

  constructor(
    private anchorService: AnchorService,
  ) { }

  ngOnInit(): void {
  }

  getRouteAnimation(outlet: RouterOutlet): string {
    return outlet
      && outlet.activatedRouteData
      && outlet.activatedRouteData.label;
  }

  handleFragment(): void {
    this.anchorService.scrollToAnchor();
  }

}
