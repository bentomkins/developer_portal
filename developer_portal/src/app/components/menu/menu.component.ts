import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { NavItem } from '../../nav-item';
import NavMenu from '../../navigation.json';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  private readonly stickyClassName = 'mat-tab-nav-bar--sticky';
  navItems: NavItem[] = NavMenu;
  routes: Route[];

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.routes = this.router.config.filter(route => route.data && route.data.label);
  }

}
